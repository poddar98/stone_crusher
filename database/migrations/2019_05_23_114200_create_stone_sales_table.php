<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoneSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stone_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('stone_name_id');
            $table->double('cft',8,2);
            $table->double('per_price',8,2);
            $table->double('total_amount',8,2);
            $table->text('comments');
            $table->tinyInteger('status')->default(1);
            $table->date('sale_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stone_sales');
    }
}
