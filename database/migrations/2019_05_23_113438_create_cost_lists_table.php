<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cost_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->double('qty',8,2);
            $table->double('per_price',8,2);
            $table->double('total_amount',8,2);
            $table->string('voucher_pic')->nullable();
            $table->date('purchase_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cost_lists');
    }
}
