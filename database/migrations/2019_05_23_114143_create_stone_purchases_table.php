<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStonePurchasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stone_purchases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id');
            $table->integer('stone_name_id');
            $table->double('cft',8,2);
            $table->double('per_price',8,2);
            $table->double('total_amount',8,2);
            $table->string('truck_number')->nullable();
            $table->string('comments')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->date('purchase_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stone_purchases');
    }
}
