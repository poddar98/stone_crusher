<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupplierDueBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_due_books', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id');
            $table->tinyInteger('type')->comment('1=due, 2=payment, 3=cash');
            $table->integer('stone_purchases_id')->nullable();
            $table->double('amount',8, 2);
            $table->date('payment_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_due_books');
    }
}
