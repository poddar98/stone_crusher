<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StonePurchase extends Model
{
    //
    protected  $guarded = [ ];

    public function suppliers() {
        return $this->belongsTo(SupplierList::class,'supplier_id');
    }

    public function stoneNames() {
        return $this->belongsTo(StoneNameList::class,'stone_name_id');
    }
}
