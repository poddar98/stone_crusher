<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StoneSale extends Model
{
    //
    protected  $guarded = [ ];

    public function clients() {
        return $this->belongsTo(ClientList::class,'client_id');
    }

    public function stoneNames() {
        return $this->belongsTo(StoneNameList::class,'stone_name_id');
    }
}
