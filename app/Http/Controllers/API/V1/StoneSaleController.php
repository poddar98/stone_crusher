<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;
use App\Models\StoneSale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StoneSaleController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['stone_sales'] = StoneSale::where('status',1)->get();
        if($request->has('month') && $request->month) {
            /*$data['stone_sales'] = StoneSale::*/
        }
        return $this->sendResponse($data,'Successfully get data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'client_id' => 'required|exists:client_lists,id',
                'stone_name_id' => 'required|exists:stone_name_lists,id',
                'cft' => 'required',
                'per_price' => 'required',
                'sale_date' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $input = $this->manageInput($request);
            $result['stone_sale'] = StoneSale::create($input);
            return $this->sendResponse($result,'successfully  created.');
        } catch (\Exception $exception) {
            \Log::error('stone sale api error :'. $exception);
            $message = ($exception->getMessage()) ? $exception->getMessage() : 'Something is went wrong';
            return $this->sendError($message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $stoneSale = StoneSale::findOrFail($id);

            $input = $this->manageInput($request,$stoneSale);
            $result['stone_sale'] = $stoneSale->update($input);
            return $this->sendResponse($result,'successfully  updated.');
        } catch (\Exception $exception) {
            \Log::error('stone sale api error :'. $exception);
            $message = ($exception->getMessage()) ? $exception->getMessage() : 'Something is went wrong';
            return $this->sendError($message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {

            $stoneSale = StoneSale::findOrFail($id);
            $result = $stoneSale->delete();
            return $this->sendResponse($result,'successfully  deleted.');
        } catch (\Exception $exception) {
            \Log::error('stone sale api error :'. $exception);
            $message = ($exception->getMessage()) ? $exception->getMessage() : 'Something is went wrong';
            return $this->sendError($message);
        }
    }

    private function manageInput($request,$stone_sale=null) {
        $input = array();

        if($request->has('client_id')) {
            $input['client_id'] = $request->client_id;
        }

        if($request->has('stone_name_id')) {
            $input['stone_name_id'] = $request->stone_name_id;
        }

        if($request->has('cft')) {
            $input['cft'] = $request->cft;
        }elseif ($stone_sale) {
            $input['cft'] = $request->cft;
        }

        if($request->has('per_price')) {
            $input['per_price'] = $request->per_price;
        }elseif ($stone_sale) {
            $input['per_price'] = $request->per_price;
        }

        $input['total_amount'] = ($input['cft'] * $input['per_price']);



        if($request->has('comments')) {
            $input['comments'] = $request->comments;
        }

        if($request->has('status')) {
            $input['status'] = $request->status;
        }

        if($request->has('sale_date')) {
            $input['sale_date'] = $request->sale_date;
        }

        return $input;
    }
}
