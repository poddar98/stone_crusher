<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;

use App\Models\Auth\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class UserController extends BaseController
{

    public $successStatus = 200;

    /**
     * old login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        $validator = Validator::make($request->all(), [
            'mobile_no' => 'required|exists:users,mobile_no',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $error = $validator->errors();
            return $this->sendError('Validation errors',$error,401);
        }

        if(Auth::attempt(['mobile_no' => request('mobile_no'), 'password' => request('password')])){

                $user = auth()->user();
                return $this->sendResponse( $user,'valid user');

        }
        else{
            return $this->sendError('Unauthorised', '',401);
        }

    }




    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'email',
            'mobile_no' => 'required',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this-> successStatus);
    }

}
