<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;
use App\Models\StonePurchase;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class StonePurchaseController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['stone_purchases'] = StonePurchase::where('status',1)->get();
        return $this->sendResponse($data,'Successfully get data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'supplier_id' => 'required|exists:supplier_lists,id',
                'stone_name_id' => 'required|exists:stone_name_lists,id',
                'cft' => 'required',
                'per_price' => 'required',
                'purchase_date' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $input = $this->manageInput($request);
            $result['stone_purchase'] = StonePurchase::create($input);
            return $this->sendResponse($result,'successfully  created.');
        } catch (\Exception $exception) {
            \Log::error('stone purchase api error :'. $exception);
            $message = ($exception->getMessage()) ? $exception->getMessage() : 'Something is went wrong';
            return $this->sendError($message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $stonePurchase = StonePurchase::findOrFail($id);

            $input = $this->manageInput($request,$stonePurchase);
            $result['stone_purchase'] = $stonePurchase->update($input);
            return $this->sendResponse($result,'successfully  updated.');
        } catch (\Exception $exception) {
            \Log::error('stone purchase api error :'. $exception);
            $message = ($exception->getMessage()) ? $exception->getMessage() : 'Something is went wrong';
            return $this->sendError($message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {

            $stonePurchase = StonePurchase::findOrFail($id);
            $result = $stonePurchase->delete();
            return $this->sendResponse($result,'successfully  deleted.');
        } catch (\Exception $exception) {
            \Log::error('stone purchase api error :'. $exception);
            $message = ($exception->getMessage()) ? $exception->getMessage() : 'Something is went wrong';
            return $this->sendError($message);
        }
    }

    private function manageInput($request,$stone_purchase=null) {
        $input = array();

        if($request->has('supplier_id')) {
            $input['supplier_id'] = $request->supplier_id;
        }

        if($request->has('stone_name_id')) {
            $input['stone_name_id'] = $request->stone_name_id;
        }

        if($request->has('cft')) {
            $input['cft'] = $request->cft;
        } elseif ($stone_purchase) {
            $input['cft'] = $stone_purchase->cft;
        }

        if($request->has('per_price')) {
            $input['per_price'] = $request->per_price;

        } elseif ($stone_purchase) {
            $input['per_price'] = $stone_purchase->per_price;
        }
        $input['total_amount'] = ($input['cft'] * $input['per_price']);

        if($request->has('truck_number')) {
            $input['truck_number'] = $request->truck_number;
        }

        if($request->has('comments')) {
            $input['comments'] = $request->comments;
        }

        if($request->has('status')) {
            $input['status'] = $request->status;
        }

        if($request->has('purchase_date')) {
            $input['purchase_date'] = $request->purchase_date;
        }

        return $input;
    }
}
