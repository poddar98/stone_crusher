<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\BaseController;
use App\Models\CostList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CostController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['costList'] = CostList::all();
        return $this->sendResponse($data,'successfully get data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'title' => 'required',
                'qty' => 'required',
                'per_price' => 'required',
                'purchase_date' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $input = $this->manageInput($request);
            $result['cost_list'] = CostList::create($input);
            return $this->sendResponse($result,'successfully  created.');
        } catch (\Exception $exception) {
            dd($exception);
            \Log::error('cost list api error :'. $exception);
            $message = ($exception->getMessage()) ? $exception->getMessage() : 'Something is went wrong';
            return $this->sendError($message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resocost_list *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {

            $cost = CostList::findOrFail($id);

            $input = $this->manageInput($request,$cost);
            $result['cost_list'] = $cost->update($input);
            return $this->sendResponse($result,'successfully  updated.');
        } catch (\Exception $exception) {
            \Log::error('cost list api error :'. $exception);
            $message = ($exception->getMessage()) ? $exception->getMessage() : 'Something is went wrong';
            return $this->sendError($message);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        try {

            $cost = CostList::findOrFail($id);
            $result = $cost->delete();
            return $this->sendResponse($result,'successfully  deleted.');
        } catch (\Exception $exception) {
            \Log::error('cost list api error :'. $exception);
            $message = ($exception->getMessage()) ? $exception->getMessage() : 'Something is went wrong';
            return $this->sendError($message);
        }
    }

    private function manageInput($request,$cost=null) {
        $input = array();

        if($request->has('title')) {
            $input['title'] = $request->title;
        }elseif($cost) {
            $input['title'] = $cost->title;
        }
        //dd($request->all());
        if($request->has('qty')) {
            $input['qty'] = $request->qty;
        }elseif($cost) {
            $input['qty'] = $cost->qty;
        }

        if($request->has('per_price')) {
            $input['per_price'] = $request->per_price;
        } elseif($cost) {
            $input['per_price'] = $cost->per_price;
        }

        $input['total_amount'] = ($input['qty'] * $input['per_price']);


        if($request->has('purchase_date')) {
            $input['purchase_date'] = $request->purchase_date;
        }

        if($request->has('voucher_pic')) {
            $input['voucher_pic'] = $request->voucher_pic;
        }
        return $input;
    }
}
