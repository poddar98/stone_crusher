<?php

namespace App\Http\Controllers\Backend;

use App\Models\CostList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $costs = CostList::orderBy('purchase_date', 'DESC');
      //  dd($costs);
        $search = false;
        if($request->has('start_date') && $request->start_date) {
            $search = true;
            $costs->whereDate('purchase_date','>=',$request->start_date);
        }
        if($request->has('end_date') && $request->end_date) {
            $search = true;
            $costs->whereDate('purchase_date','<=',$request->end_date);
        }
        $input = $request->all();

        if($search && $input && count($input)) {
            $data['costs'] = $costs->get();
            $data['request'] = $request->all();
            $costs_data = array_column($data['costs']->toArray(),'total_amount');
            $data['total_cost_amount'] = array_sum($costs_data);
        } else {
            $data['costs'] = $costs->paginate(5);
        }
        return view('backend.cost.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.cost.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $this->manageInput($request);
            CostList::create($input);
            return redirect()->route('admin.cost.index')->withFlashSuccess(__('alerts.backend.cost.created'));
        } catch (\Exception $exception) {
            dd($exception);
            return redirect()->back()->withFlashDanger('something went wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['cost'] = CostList::findOrFail($id);
        return view('backend.cost.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $cost = CostList::findOrFail($id);
            $input = $this->manageInput($request, $cost);

            $cost->update($input);
            return redirect()->route('admin.cost.index')->withFlashSuccess(__('alerts.backend.cost.updated'));
        } catch (\Exception $exception) {
            dd($exception);
            return redirect()->back()->withFlashDanger('something went wrong');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function manageInput($request,$cost=null) {
        $input = array();

        if($request->has('title')) {
            $input['title'] = $request->title;
        }

        if($request->has('type')) {
            $input['type'] = $request->type;
        }

        if($request->has('qty')) {
            $input['qty'] = $request->qty;
        }elseif ($cost) {
            $input['qty'] = $request->qty;
        }


        if($request->has('per_price')) {
            $input['per_price'] = $request->per_price;
        }elseif ($cost) {
            $input['per_price'] = $request->per_price;
        }

        $input['total_amount'] = ($input['qty'] * $input['per_price']);



        if($request->has('comments')) {
            $input['comments'] = $request->comments;
        }

        if($request->file('voucher_pic')) {
            /*$this->validate($request, [

                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

            ]);*/


            $image = $request->file('voucher_pic');


            $input['voucher_pic'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/cost_image');

            $image->move($destinationPath, $input['voucher_pic']);
        }

        if($request->has('purchase_date')) {
            $input['purchase_date'] = $request->purchase_date;
        }

        return $input;
    }
}
