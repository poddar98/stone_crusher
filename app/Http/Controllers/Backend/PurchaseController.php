<?php

namespace App\Http\Controllers\Backend;

use App\Models\StoneNameList;
use App\Models\StonePurchase;
use App\Models\SupplierList;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $purchases = StonePurchase::orderBy('purchase_date', 'DESC');
        $search = false;
        if($request->has('start_date') && $request->start_date) {
            $search = true;
            $purchases->whereDate('purchase_date','>=',$request->start_date);
        }
        if($request->has('end_date') && $request->end_date) {
            $search = true;
            $purchases->whereDate('purchase_date','<=',$request->end_date);
        }
        $input = $request->all();

        if($search) {
            $data['purchases'] = $purchases->get();
            $data['request'] = $request->all();
            $purchase_data = array_column($data['purchases']->toArray(),'total_amount');
            $data['total_purchase_amount'] = array_sum($purchase_data);
        } else {
            $data['purchases'] = $purchases->paginate(5);
        }
        return view('backend.purchase.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['suppliers'] = SupplierList::where('status',1)->pluck('name','id')->toArray();

        $data['stonenames'] = StoneNameList::where('status',1)->pluck('name','id')->toArray();

        return view('backend.purchase.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $this->manageInput($request);
            StonePurchase::create($input);
            return redirect()->route('admin.purchase.index')->withFlashSuccess(__('alerts.backend.purchase.created'));
        } catch (\Exception $exception) {
            dd($exception);
            return redirect()->back()->withFlashDanger('something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['purchase'] = StonePurchase::findOrFail($id);
       $data['suppliers'] = SupplierList::where('status',1)->pluck('name','id')->toArray();

       $data['stonenames'] = StoneNameList::where('status',1)->pluck('name','id')->toArray();

       return view('backend.purchase.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $purchase = StonePurchase::findOrFail($id);
            $input = $this->manageInput($request, $purchase);

            $purchase->update($input);
            return redirect()->route('admin.purchase.index')->withFlashSuccess(__('alerts.backend.purchase.updated'));
        } catch (\Exception $exception) {
            return redirect()->back()->withFlashDanger('something went wrong');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function manageInput($request,$stone_purchase=null) {
        $input = array();

        if($request->has('supplier_id')) {
            $input['supplier_id'] = $request->supplier_id;
        }

        if($request->has('stone_name_id')) {
            $input['stone_name_id'] = $request->stone_name_id;
        }

        if($request->has('cft')) {
            $input['cft'] = $request->cft;
        } elseif ($stone_purchase) {
            $input['cft'] = $stone_purchase->cft;
        }

        if($request->has('per_price')) {
            $input['per_price'] = $request->per_price;

        } elseif ($stone_purchase) {
            $input['per_price'] = $stone_purchase->per_price;
        }
        $input['total_amount'] = ($input['cft'] * $input['per_price']);

        if($request->has('truck_number')) {
            $input['truck_number'] = $request->truck_number;
        }

        if($request->has('comments')) {
            $input['comments'] = $request->comments;
        }

        if($request->has('status')) {
            $input['status'] = $request->status;
        }

        if($request->has('purchase_date')) {
            $input['purchase_date'] = $request->purchase_date;
        }

        return $input;
    }
}
