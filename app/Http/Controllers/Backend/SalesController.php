<?php

namespace App\Http\Controllers\Backend;

use App\Models\ClientList;
use App\Models\StoneNameList;
use App\Models\StoneSale;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sales = StoneSale::orderBy('sale_date','DESC');
        $search = false;
        if($request->has('start_date') && $request->start_date) {
            $search = true;
            $sales->whereDate('sale_date','>=',$request->start_date);
        }
        if($request->has('end_date') && $request->end_date) {
            $search = true;
            $sales->whereDate('sale_date','<=',$request->end_date);
        }
        $input = $request->all();

        if($search && $input && count($input)) {
            $data['sales'] = $sales->get();
            $data['request'] = $request->all();
            $sales_data = array_column($data['sales']->toArray(),'total_amount');
            $data['total_sale_amount'] = array_sum($sales_data);
        } else {
        $data['sales'] = $sales->paginate(5);
        }
        return view('backend.sale.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['clients'] = ClientList::where('status',1)->pluck('name','id')->toArray();
        $data['stonenames'] = StoneNameList::where('status',1)->pluck('name','id')->toArray();
        return view('backend.sale.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $input = $this->manageInput($request);
            StoneSale::create($input);
            return redirect()->route('admin.sale.index')->withFlashSuccess(__('alerts.backend.sale.created'));
        } catch (\Exception $exception) {
            dd($exception);
            return redirect()->back()->withFlashDanger('something went wrong');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['sale'] = StoneSale::findOrFail($id);
        $data['clients'] = ClientList::where('status',1)->pluck('name','id')->toArray();
        $data['stonenames'] = StoneNameList::where('status',1)->pluck('name','id')->toArray();
        return view('backend.sale.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $stoneSale = StoneSale::findOrFail($id);
            $input = $this->manageInput($request, $stoneSale);

            $stoneSale->update($input);
            return redirect()->route('admin.sale.index')->withFlashSuccess(__('alerts.backend.sale.updated'));
        } catch (\Exception $exception) {
            dd($exception);
            return redirect()->back()->withFlashDanger('something went wrong');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function manageInput($request,$stone_sale=null) {
        $input = array();

        if($request->has('client_id')) {
            $input['client_id'] = $request->client_id;
        }

        if($request->has('stone_name_id')) {
            $input['stone_name_id'] = $request->stone_name_id;
        }

        if($request->has('cft')) {
            $input['cft'] = $request->cft;
        }elseif ($stone_sale) {
            $input['cft'] = $request->cft;
        }

        if($request->has('per_price')) {
            $input['per_price'] = $request->per_price;
        }elseif ($stone_sale) {
            $input['per_price'] = $request->per_price;
        }

        $input['total_amount'] = ($input['cft'] * $input['per_price']);



        if($request->has('comments')) {
            $input['comments'] = $request->comments;
        }

        if($request->has('status')) {
            $input['status'] = $request->status;
        }

        if($request->has('sale_date')) {
            $input['sale_date'] = $request->sale_date;
        }

        return $input;
    }
}
