<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.purchase.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('Purchase Management'), route('admin.purchase.index'));
});


Breadcrumbs::for('admin.purchase.edit', function ($trail, $id) {
    $trail->parent('admin.purchase.index');
    $trail->push(__('Purchase Edit'), route('admin.purchase.edit', $id));
});

Breadcrumbs::for('admin.purchase.create', function ($trail) {
    $trail->parent('admin.purchase.index');
    $trail->push(__('Purchase Create'), route('admin.purchase.create'));
});


Breadcrumbs::for('admin.sale.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('Sale Management'), route('admin.sale.index'));
});


Breadcrumbs::for('admin.sale.edit', function ($trail, $id) {
    $trail->parent('admin.sale.index');
    $trail->push(__('Sale Edit'), route('admin.sale.edit', $id));
});

Breadcrumbs::for('admin.sale.create', function ($trail) {
    $trail->parent('admin.sale.index');
    $trail->push(__('Sale Create'), route('admin.sale.create'));
});

Breadcrumbs::for('admin.cost.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('Cost Management'), route('admin.cost.index'));
});


Breadcrumbs::for('admin.cost.edit', function ($trail, $id) {
    $trail->parent('admin.cost.index');
    $trail->push(__('Cost Edit'), route('admin.cost.edit', $id));
});

Breadcrumbs::for('admin.cost.create', function ($trail) {
    $trail->parent('admin.cost.index');
    $trail->push(__('Cost Create'), route('admin.cost.create'));
});


require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
