<?php

Route::group(['namespace' => 'V1'], function () {

    Route::post('login', 'UserController@login');

    Route::post('register', 'UserController@register');

    Route::resource('suppliers','SupplierController');

    Route::resource('stone-purchases','StonePurchaseController');

    Route::resource('stone-names','StoneNameController');

    Route::resource('clients','ClientController');

    Route::resource('stone-sales','StoneSaleController');

    Route::resource('costs','CostController');

    Route::group(['middleware' => 'auth:api'], function(){

    });

});
