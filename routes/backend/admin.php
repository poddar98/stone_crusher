<?php

/**
 * All route names are prefixed with 'admin.'.
 */
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', 'DashboardController@index')->name('dashboard');

Route::resource('purchases', 'PurchaseController')->names('purchase');
Route::resource('sales','SalesController')->names('sale');

Route::resource('costs','CostController')->names('cost');
