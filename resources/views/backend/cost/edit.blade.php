@extends('backend.layouts.app')

@section('title', __('Cost Management') . ' | ' . __('Sales Edit'))


@section('content')
    {{ html()->modelForm($cost, 'PATCH', route('admin.cost.update', $cost->id))->acceptsFiles()->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('Cost Management')
                        <small class="text-muted">@lang('edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            @include('backend.cost.form')
        </div>
    {{--</div>
        </div>--}}<!--card body-->
    <div class="card-footer">
        <div class="row">
            <div class="col">
                {{ form_cancel(route('admin.cost.index'), __('buttons.general.cancel')) }}
            </div><!--col-->

            <div class="col text-right">
                {{ form_submit(__('buttons.general.crud.update')) }}
            </div><!--row-->
        </div><!--row-->
    </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection
@push('after-scripts')
    <script>

        changetotalamount();

        $('.cft').on('change', function () {
            changetotalamount();
        });
        $('.per_price').on('change', function () {
            changetotalamount();
        });
        function changetotalamount() {
            var totalAmount = $('.cft').val()*$('.per_price').val();
            $('.total_amount').val(totalAmount);
            console.log(totalAmount);
        }
    </script>
@endpush
