@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.users.management'))



@section('content')
    <div class="card">
        <div class="card-body">
            {{ html()->form('get',route('admin.cost.index'))->open() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <label for="Start Date">Start Date</label>
                            {{ html()->date('start_date',(isset($request['start_date']) ? $request['start_date'] : null ))->class('form-control search-slt') }}
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-12">
                            <label for="End Date">End Date</label>
                            {{ html()->date('end_date',(isset($request['end_date']) ? $request['end_date'] : null ))->class('form-control search-slt') }}
                        </div>

                    </div>
                    {{ form_submit(__('search'))->class('mt-1') }}



                </div>

            </div>
            {{ html()->form()->close() }}


        </div>
    </div>
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('Cost Management') }} <small class="text-muted">{{ __('Cost List') }}</small>
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                    <div class="btn-toolbar float-right" role="toolbar" aria-label="@lang('labels.general.toolbar_btn_groups')">
                        <a href="{{ route('admin.cost.create') }}" class="btn btn-success ml-1" data-toggle="tooltip" title="@lang('labels.general.create_new')"><i class="fas fa-plus-circle"></i></a>
                    </div><!--btn-toolbar-->
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>@lang('Title')</th>
                                <th>@lang('Type')</th>
                                <th>@lang('Qty')</th>
                                <th>@lang('Per Price')</th>
                                <th>@lang('Total Amount')</th>
                                <th>@lang('Purchase Date')</th>
                                <th>@lang('Voucher Pic')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($costs as $cost)
                                <tr>
                                    <td>{{ $cost->title ?? ' ' }}</td>
                                    <td>{{ $cost->type ?? ' ' }}</td>
                                    <td>{{ $cost->qty or ' ' }}</td>
                                    <td>{!! $cost->per_price  or ' ' !!}</td>
                                    <td>{!! $cost->total_amount or ' ' !!}</td>
                                    <td>{!! $cost->purchase_date or ' ' !!}</td>
                                    <td>
                                        @if($cost->voucher_pic)
                                        <img src="{!! asset('cost_image/'.$cost->voucher_pic) !!}" width="100px" height="100px" alt="">
                                        @else
                                            Not Given
                                        @endif

                                    </td>

                                    <td>
                                        <a href="{!! route('admin.cost.edit', $cost) !!}" data-toggle="tooltip" data-placement="top" title="'.__('buttons.general.crud.edit').'" class="btn btn-primary"><i class="fas fa-edit"></i></a>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        @if(isset($total_cost_amount))
                            <strong>Total Cost Amount : {{ $total_cost_amount }}</strong>
                            @else
                            {!! $costs->total() !!} {{ trans_choice('costs', $costs->total()) }}
                        @endif
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                        @if(! isset($total_cost_amount))
                            {!! $costs->render() !!}
                        @endif

                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection
