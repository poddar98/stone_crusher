<div class="row mt-4 mb-4">
    <div class="col">

        <div class="form-group row">
            {{ html()->label(__('Title'))->class('col-md-2 form-control-label')->for('title') }}

            <div class="col-md-10">
                {{ html()->text('title')
                    ->class('form-control title')
                    ->placeholder(__('Title'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Type'))->class('col-md-2 form-control-label')->for('type') }}

            <div class="col-md-10">
                {{ html()->select('type',['office_cost' => 'Office Cost','machine_cost' => 'Machine Cost','labour_cost' => 'Labour Cost','others_cost' => 'Others Cost'])
                    ->class('form-control title')
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Qty'))->class('col-md-2 form-control-label')->for('cft') }}

            <div class="col-md-10">
                {{ html()->text('qty')
                    ->class('form-control cft')
                    ->placeholder(__('QTY'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Per Price'))->class('col-md-2 form-control-label')->for('per_price') }}

            <div class="col-md-10">
                {{ html()->text('per_price')
                    ->class('form-control per_price')
                    ->placeholder(__('Per Price'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Total Amount'))->class('col-md-2 form-control-label')->for('total_amount') }}

            <div class="col-md-10">
                {{ html()->text('total_amount')
                    ->class('form-control total_amount')
                    ->placeholder(__('Total Amount'))
                    ->attribute('maxlength', 191)
                    ->readonly() }}
            </div><!--col-->
        </div><!--form-group-->


{{--
        <div class="form-group row">
            {{ html()->label(__('Comments'))->class('col-md-2 form-control-label')->for('comments') }}

            <div class="col-md-10">
                {{ html()->text('comments')
                    ->class('form-control')
                    ->placeholder(__('Comments'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->--}}

        {{--<div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.users.active'))->class('col-md-2 form-control-label')->for('active') }}

            <div class="col-md-10">
                <label class="switch switch-label switch-pill switch-primary">
                    {{ html()->checkbox('status', true, '1')->class('switch-input') }}
                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                </label>
            </div><!--col-->
        </div><!--form-group-->
--}}

        <div class="form-group row">
            {{ html()->label(__('Purchase Date'))->class('col-md-2 form-control-label')->for('comments') }}

            <div class="col-md-10">
                {{ html()->date('purchase_date')
                    ->class('form-control')
                    ->placeholder(__('Purchase Date'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Voucher Pic'))->class('col-md-2 form-control-label')->for('comments') }}

            <div class="col-md-10">
                {{ html()->file('voucher_pic')
                    ->class('form-control') }}
            </div><!--col-->
        </div><!--form-group-->



    </div><!--col-->
</div><!--form-group-->
