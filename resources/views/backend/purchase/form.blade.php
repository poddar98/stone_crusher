<div class="row mt-4 mb-4">
    <div class="col">
        <div class="form-group row">
            {{ html()->label(__('Supplier'))->class('col-md-2 form-control-label')->for('supplier_id') }}

            <div class="col-md-10">
                {{--{{ html()->text('supplier_id')
                    ->class('form-control')
                    ->placeholder(__('supplier'))
                    ->attribute('maxlength', 191)
                    ->required() }}--}}
                {{ html()->select('supplier_id',$suppliers)->class('form-control')->placeholder('select a supplier')->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Stone Name'))->class('col-md-2 form-control-label')->for('stone_name_id') }}

            <div class="col-md-10">
                {{ html()->select('stone_name_id',$stonenames)->class('form-control')->placeholder('select a stone_name')->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Cft'))->class('col-md-2 form-control-label')->for('cft') }}

            <div class="col-md-10">
                {{ html()->text('cft')
                    ->class('form-control cft')
                    ->placeholder(__('CFT'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Per Price'))->class('col-md-2 form-control-label')->for('per_price') }}

            <div class="col-md-10">
                {{ html()->text('per_price')
                    ->class('form-control per_price')
                    ->placeholder(__('Per Price'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Total Amount'))->class('col-md-2 form-control-label')->for('total_amount') }}

            <div class="col-md-10">
                {{ html()->text('total_amount')
                    ->class('form-control total_amount')
                    ->placeholder(__('Total Amount'))
                    ->attribute('maxlength', 191)
                    ->readonly() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Truck Number'))->class('col-md-2 form-control-label')->for('truck_number') }}

            <div class="col-md-10">
                {{ html()->text('truck_number')
                    ->class('form-control')
                    ->placeholder(__('Truck Number'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('Comments'))->class('col-md-2 form-control-label')->for('comments') }}

            <div class="col-md-10">
                {{ html()->text('comments')
                    ->class('form-control')
                    ->placeholder(__('Comments'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->

        <div class="form-group row">
            {{ html()->label(__('validation.attributes.backend.access.users.active'))->class('col-md-2 form-control-label')->for('active') }}

            <div class="col-md-10">
                <label class="switch switch-label switch-pill switch-primary">
                    {{ html()->checkbox('status', true, '1')->class('switch-input') }}
                    <span class="switch-slider" data-checked="yes" data-unchecked="no"></span>
                </label>
            </div><!--col-->
        </div><!--form-group-->


        <div class="form-group row">
            {{ html()->label(__('Purchase Date'))->class('col-md-2 form-control-label')->for('comments') }}

            <div class="col-md-10">
                {{ html()->date('purchase_date')
                    ->class('form-control')
                    ->placeholder(__('Purchase Date'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--col-->
        </div><!--form-group-->



    </div><!--col-->
</div><!--form-group-->
