@extends('backend.layouts.app')

@section('title', __('Purcahse Management') . ' | ' . __('Purchase Edit'))


@section('content')
    {{ html()->modelForm($purchase, 'PATCH', route('admin.purchase.update', $purchase->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('Purchase Management')
                        <small class="text-muted">@lang('edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            @include('backend.purchase.form')
        </div><!--col-->


        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.purchase.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    {{ form_submit(__('buttons.general.crud.update')) }}
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection
@push('after-scripts')
    <script>

        changetotalamount();

        $('.cft').on('change', function () {
            changetotalamount();
        });
        $('.per_price').on('change', function () {
            changetotalamount();
        });
        function changetotalamount() {
            var totalAmount = $('.cft').val()*$('.per_price').val();
            $('.total_amount').val(totalAmount);
            console.log(totalAmount);
        }
    </script>
@endpush
