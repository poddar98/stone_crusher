<footer class="app-footer">
    <div>
        <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }} <a href="#">{!! env('APP_NAME') !!}</a></strong>
    </div>

    <div class="ml-auto">Powered by <a href="http://coreui.io">CoreUI</a></div>
</footer>
